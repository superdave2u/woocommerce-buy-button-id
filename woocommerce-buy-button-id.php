<?php
/*
* Plugin Name: Woocommerce Buy Button Shortcode
* Description: Shortcode for buy button by ID
* Author: Dave Mainville
* Author Uri: http://superdave2u.com
* 
*/
class WMBuyButton {
    function __construct() {
		// add_shortcode()
		add_shortcode('product_button',array($this,'shortcode_func'));
    }

    // [product_button id="" sku="CNW"]
    function shortcode_func($atts, $content = null ) {
    	extract( shortcode_atts( array(
	    	'id' 	=> null,
	    	'sku' 	=> null,
	    	'text'	=> null
		), $atts ) );


		if(!is_null($id)) {
			$product = new WC_Product($id);
		}

		if(!is_null($sku)) {
			$product = $this->get_product_by_sku($sku);
		}

		if(!empty($product)) {

			$link_text = (is_null($text)) ? $product->single_add_to_cart_text() : 'Book Now' ;
			// <a href="http://dcdev.webmanna.com/product/course-night-workshop/" rel="nofollow" data-product_id="227" data-product_sku="CNW" class=" button product_type_variable">Book Now</a>
			$output = '<span class="woocommerce"><a href="' . get_permalink($product->id) . '" rel="nofollow" data-product_id="' . $product->id . '" data-product_sku="' . $product->get_sku() . '" class=" button product_type_' . $product->product_type . '">' . $link_text . '</a></span>';
			return $output;
		}

		return "Shortcode button has no valid inputs";
    }

	function get_product_by_sku( $sku ) {
		
		global $wpdb;
		
		$product_id = $wpdb->get_var( $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key='_sku' AND meta_value='%s' LIMIT 1", $sku ) );
		
		if ( $product_id ) return new WC_Product( $product_id );
		
		return null;
	}    
}
$WMBuyButton = new WMBuyButton();
?>
